import * as util from 'util';
const passport = require('passport');

const { Strategy } = passport;


/**
 * `ResourceOwnerPasswordStrategy` constructor.
 *
 * @api protected
 */
function ResourceOwnerStrategy(options, verify) {
  if (typeof options === 'function') {
    verify = options;
    options = {};
  }

  if (!verify) {
    throw new Error('OAuth 2.0 resource owner password strategy requires a verify function');
  }

  Strategy.call(this);

  this.name = 'oauth2-resource-owner-password';
  this._verify = verify;
  this._passReqToCallback = options.passReqToCallback;
}

/**
 * Inherit from `Strategy`.
 */
util.inherits(ResourceOwnerStrategy, Strategy);

/**
 * Authenticate request based on client credentials in the request body.
 *
 * @param {Object} req
 * @api protected
 */
ResourceOwnerStrategy.prototype.authenticate = function(req) {
  if (!req.body) {
    return this.fail();
  }
  const clientId = req.body['client_id'];
  const clientSecret = req.body['client_secret'];
  const username = req.body['username'];
  const password = req.body['password'];
  const grantType = req.body['grant_type'];
  const self = this;

  if (grantType !== 'password') {
    return this.fail();
  }
  if (!username || !password) {
    return this.fail();
  }

  function verified(err, client, user, info) {
    if (err) { return self.error(err); }
    if (!client) { return self.fail(); }
    if (!user) { return self.fail(); }
    self.success(client, user, info);
  }

  if (self._passReqToCallback) {
    this._verify(req, clientId, clientSecret, username, password, verified);
  } else {
    this._verify(clientId, clientSecret, username, password, verified);
  }
};


/**
 * Expose `Strategy`.
 */
export { ResourceOwnerStrategy };
export default ResourceOwnerStrategy;
