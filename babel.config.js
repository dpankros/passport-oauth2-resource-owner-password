const plugins = [];
const presets = [];
const ignore = [];
const otherOptions = {
  comments: true,
};
//
// Common options
//
presets.push([
  '@babel/preset-env',
  { targets: { node: 'current' } }
]);

plugins.push('@babel/plugin-transform-runtime');

//
// Production-only options
//

if (process.env.BABEL_ENV === 'production') {
  presets.push('minify');
  otherOptions.comments = false;
  otherOptions.compact = true;
}

module.exports = {
  ...otherOptions,
  presets,
  plugins,
  ignore,
};
