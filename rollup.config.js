import json from '@rollup/plugin-json';
import babel from '@rollup/plugin-babel';
import resolve from '@rollup/plugin-node-resolve';
import pkg from './package.json';


export default [
  {
    input: 'src/index.js',
    external: Object.keys(pkg.peerDependencies || {}),
    output: [
      { file: './lib/passport-oauth2-resource-owner-password.js', format: 'cjs', sourcemap: true, exports: 'named' },
      { file: './lib/passport-oauth2-resource-owner-password.esm.js', format: 'es', sourcemap: true },
    ],
    plugins: [
      json(),

      resolve(),

      babel({ babelHelpers: 'runtime' }),
    ]
  }
];
